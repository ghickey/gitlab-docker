# gitlab-docker

[![pipeline status](https://gitlab.com/ghickey/gitlab-docker/badges/master/pipeline.svg)](https://gitlab.com/ghickey/gitlab-docker/commits/master)

Base Docker container for interacting with GitLab API.

This docker container should be the base image for any scripting that needs
to interact with the GitLab API. This can simply be used by adding the
following to the top of a Dockerfile. 

```
FROM registry.gitlab.com/ghickey/gitlab-docker:VERSION
```
